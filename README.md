Ansible Role: Graylog2
============
Setup Graylog2 (Graylog2, Elasticsearch, RabbitMQ, Logstash)

Tags
--------
| Tags | Description |  
|-|-|
| install_rabbitmq | Install rabbitmq-server 3.7.7  |   
| install_mongodb | Install MongoDB 3.6 |  
| install_elasticsearch | Install Elasticsearch cluster 6.3.2 |
| install_graylog | Install Graylog 2.5.1 |
| install_logstash | Install Logstash 6.3.2 |
| config_logstash | Config Logstash |
| setup | Tag `install_mongodb` + `install_elasticsearch` + `install_graylog` + `install_rabbitmq` + `install_logstash`|

Example Playbook
----------------
```
ansible-playbook -Di inventory/non-prod -u longdv play.yml --tags="setup"
```
