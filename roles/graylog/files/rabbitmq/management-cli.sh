#!/bin/bash
user="eq-dev eq-qa eq-staging eq-performance"
pass="gXlmqnXuE96o1zT6 ZhCUitI52eOcs0sD CWuXWEeZlAlIvfEr jKcww4YyqMI5oxYv"
host="vn1n-rabbitmq-mgt01.ascendmoney-dev.internal"
node="vn1n-rabbitmq-node01 vn1n-rabbitmq-node02"
exchange=graylog
exchange_type=direct
function rabbitmq_cli(){
/usr/local/bin/rabbitmqadmin --ssl -k --host=$host --port=15672 \
${@}
}

function set_queues(){
set -f
while [  "$user" ]
do
    set -- $user;i=$1; shift; user=$*
    set -- $pass;j=$1; shift; pass=$*
  for b in ${node}
  do
        for a in `cat queue_${b}.txt`
        do
                rabbitmq_cli declare queue --username="$i" \
                --password=$j \
                --vhost="/$i" \
                node=rabbit@${b} \
                name="${a}" \
                durable=true
       done
  done
done
}

function delete_queues(){
set -f
while [  "$user" ]
do
    set -- $user;i=$1; shift; user=$*
    set -- $pass;j=$1; shift; pass=$*
        for a in `cat queue.txt`
        do
                rabbitmq_cli delete queue --username="$i" \
                --password=$j \
                --vhost="/$i" \
                name="${a}"
        done
done
}

function set_exchange() {
        rabbitmq_cli declare exchange  --username=admin \
        --password=jgUwQffhRrifNMFypcp3 \
        name=$exchange type=$exchange_type
}


#set_queues
#delete_queues
#set_exchange
